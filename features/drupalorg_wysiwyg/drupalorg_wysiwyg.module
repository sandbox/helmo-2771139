<?php

/**
 * Implements hook_element_info_alter().
 * Sets the default format if the user's default format is filtered_html.
 */
function drupalorg_wysiwyg_element_info_alter(&$type) {
  array_unshift($type['text_format']['#pre_render'], 'drupalorg_wysiwyg_process_filter_format');
}

/**
 * Callback function to process the filter format and remove the fieldset.
 * More info:  http://drupal.org/node/1949552
 */
function drupalorg_wysiwyg_process_filter_format($element) {
  // If no format is found, exit.
  if (!isset($element['format'])) {
    return $element;
  }

  // By default, disable ckeditor.
  $element['value']['#wysiwyg'] = FALSE;

  // Disable the summary value wysiwyg.
  if (isset($element['summary'])) {
    $element['summary']['#wysiwyg'] = FALSE;
  }

  // Disable Depricated input filter, unless its already being used
  if ($element['format']['format']['#default_value'] != '5') {
    unset($element['format']['format']['#options'][5]);
  }

  // Progressively enable ckeditor for specific entities & bundles / CTs.
  // Nodes
  if (isset($element['#entity_type']) && $element['#entity_type'] == 'node') {
    $bundles_to_enable = array('page', 'post', 'section', 'guide', 'documentation');
    if (in_array($element['#bundle'], $bundles_to_enable)) {
      $element['value']['#wysiwyg'] = TRUE;
      // Disable the summary value wysiwyg.
      if (isset($element['summary'])) {
        $element['summary']['#wysiwyg'] = TRUE;
      }
    }
  }
  return $element;
}

/**
 * Add code filter and linebreaks security filter to ckeditor.
 */
function drupalorg_wysiwyg_ckeditor_security_filter() {

  return array(
    'codefilter' => array(
      'title' => 'Code Filter',
      'project_page' => 'http://drupal.org/project/codefilter',
      'description' => 'Converts code and pre tags into formats that are readible by ckeditor',
      'weight' => 0,
      'installed' => FALSE,
      'filters' => array()
    ),
    'filter_autop' => array(
      'title' => 'Core Linebreaks',
      'project_page' => 'http://drupal.org/project/drupal',
      'description' => 'Convert line breaks into HTML (i.e. br and p)',
      'weight' => 1,
      'installed' => FALSE,
      'filters' => array()
    ),
    'filter_html_image_secure' => array(
      'title' => 'Local Images',
      'project_page' => 'http://drupal.org/project/filter_html_image_secure',
      'description' => 'Filters out external image urls',
      'weight' => 1,
      'installed' => FALSE,
      'filters' => array()
    ),
  );
}
