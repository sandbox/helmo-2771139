<?php
/**
 * @file
 * drupalorg_ecosystem.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function drupalorg_ecosystem_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-project_module-field_project_ecosystem'.
  $field_instances['node-project_module-field_project_ecosystem'] = array(
    'bundle' => 'project_module',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Use this field to indicate that this project\'s primary purpose is to provide enhancement for one or more other projects, and thus forms part of an ecosystem around it. For example, if your project provides plugins for Views, or adds functionality to Organic Groups, and so on. Projects that are referred to here will gain a list of \'ecosystem modules\' on their project page, allowing users to easily find modules to extend their functionality.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'issuemetadata' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'nodechanges' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'rss' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_project_ecosystem',
    'label' => 'Ecosystem',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => -1,
    ),
  );

  // Exported field_instance: 'node-project_theme-field_project_ecosystem'.
  $field_instances['node-project_theme-field_project_ecosystem'] = array(
    'bundle' => 'project_theme',
    'default_value' => NULL,
    'default_value_function' => '',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 25,
      ),
      'issuemetadata' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'nodechanges' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'rss' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_project_ecosystem',
    'label' => 'Ecosystem',
    'required' => 0,
    'settings' => array(
      'behaviors' => array(
        'prepopulate' => array(
          'status' => 0,
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 47,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Ecosystem');
  t('Use this field to indicate that this project\'s primary purpose is to provide enhancement for one or more other projects, and thus forms part of an ecosystem around it. For example, if your project provides plugins for Views, or adds functionality to Organic Groups, and so on. Projects that are referred to here will gain a list of \'ecosystem modules\' on their project page, allowing users to easily find modules to extend their functionality.');

  return $field_instances;
}
