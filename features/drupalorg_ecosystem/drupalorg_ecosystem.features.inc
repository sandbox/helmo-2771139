<?php
/**
 * @file
 * drupalorg_ecosystem.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupalorg_ecosystem_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
