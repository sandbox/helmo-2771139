<?php
/**
 * @file
 * drupalorg_ecosystem.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function drupalorg_ecosystem_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_project_ecosystem'.
  $field_bases['field_project_ecosystem'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_project_ecosystem',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'project_behavior',
      'handler_settings' => array(
        'behavior' => 'project',
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
